package it.com.servicerocket.plugins.jira.mercurial.pageObject;

import com.atlassian.jira.pageobjects.pages.AbstractJiraAdminPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class MercurialAddRepoPage extends AbstractJiraAdminPage {

    @ElementBy(name= "add")
    private PageElement add;

    @ElementBy(id = "displayName")
    private PageElement displayName;

    @ElementBy(id = "root")
    private SelectElement root;

    public void setRepoName(String name) {
        this.displayName.type(name);
    }

    public void setRepoRoot(String root) {
        this.root.type(root);
    }

    public void clickAdd() {
        add.click();
    }

    @Override
    public TimedCondition isAt() {
        return add.timed().isVisible();
    }

    @Override
    public String linkId() {
        return "net.customware.jira.plugins.mercurial.link";
    }

    @Override
    public String getUrl() {
        return "/secure/AddMercurialRepository!default.jspa";
    }
}
