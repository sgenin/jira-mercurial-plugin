/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Oct 1, 2004
 * Time: 5:06:44 PM
 */
package net.customware.jira.plugins.ext.mercurial.revisions;

import net.customware.jira.plugins.ext.mercurial.MercurialManager;
import net.customware.jira.plugins.ext.mercurial.MultipleMercurialRepositoryManager;
import net.customware.jira.plugins.ext.mercurial.MultipleMercurialRepositoryManagerImpl;
import net.customware.jira.plugins.ext.mercurial.reports.ReportsResource;

import com.atlassian.configurable.ObjectConfiguration;
import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.service.AbstractService;
import com.atlassian.jira.service.ServiceManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.velocity.VelocityManager;

import com.opensymphony.module.propertyset.PropertySet;

import java.util.Collection;

public class RevisionIndexService extends AbstractService
{
    public static final String REVISION_INDEX_SERVICE_NAME = "Mercurial Revision Indexing Service";
    public static final long REVISION_INDEX_SERVICE_DELAY = 60 * 60 * 1000L;
    private static MultipleMercurialRepositoryManagerImpl mmrmi;

    private String RELEASENOTES = "Release_Notes_Email";
    private String defaultEmailAddresses = "";
    private String ADDRESSEDIN = "Release_Field_Name";
    private String addressedInFieldName = "";
    private ReportsResource reportsResource;

    public RevisionIndexService(DateTimeFormatterFactory dateTimeFormatterFactory)
    {
        // Any injected classes here are all used only for the release notes.
        // They worked fine for the REST resource but not here.
        //
        // Note: trying to simply use the jersey client and the REST
        // interface fails because jersey-client classes are not accessible via
        // the class loader used for this class, as shown by
        // Package.getPackages(). Possibly because the service class
        // is created by reflection?  Various attempts at bundling the
        // required class (RuntimeDelegateImpl) didn't succeed (pom
        // instructions, bundled import, etc).  JIRA native plugins
        // such as Charting have REST resources that use an underlying
        // method for reports or gadgets, which is the approach used
        // here.
        reportsResource = new ReportsResource(null, dateTimeFormatterFactory);
    }

    public void init(PropertySet props) throws ObjectConfigurationException
    {
        super.init(props);
        if (hasProperty(RELEASENOTES))
        {
            defaultEmailAddresses = getProperty(RELEASENOTES);
        }
        if (hasProperty(ADDRESSEDIN))
        {
            addressedInFieldName = getProperty(ADDRESSEDIN);
        }
    }

    public void run()
    {
        // 159 remote repositories took about 450s, or around 3s/repo using
        // grep 'Checking for new changesets' catalina.out  | awk '{print $2, $18}' 
        log.warn(REVISION_INDEX_SERVICE_NAME + ": starting execution");
        try
        {
            MultipleMercurialRepositoryManager multipleMercurialRepositoryManager = getMultipleMercurialRepositoryManager();

            if (null == multipleMercurialRepositoryManager)
                return; // Just return --- the plugin is disabled. Don't log anything.

            if (multipleMercurialRepositoryManager.getRevisionIndexer() != null)
            {
                multipleMercurialRepositoryManager.getRevisionIndexer().updateIndex();
            }
            else
            {
                log.warn("Tried to index changes but MercurialManager has no revision indexer");
                return;
            }
            
            // Note: "none" is a valid value for defaultEmailAddresses
            // indicating that the default is to send no email
            if (defaultEmailAddresses != null && !defaultEmailAddresses.equals("")) {
                sendReleaseNotes(defaultEmailAddresses, 
                                 addressedInFieldName, 
                                 multipleMercurialRepositoryManager);
            } else {
                log.info("Release notes generation is not enabled");
            }
            
        }
        catch (Throwable t)
        {
            log.error("Error indexing changes: " + t, t);
        }
        log.warn(REVISION_INDEX_SERVICE_NAME + ": finished execution");
    }

    /** 
     * Iterate through the repositories and get the HTML release
     * notes report for each one.  
     */
    protected void sendReleaseNotes(String defaultEmailAddresses,
                                    String addressedInFieldName,
                                    MultipleMercurialRepositoryManager multipleMercurialRepositoryManager) throws Exception {
        Collection<MercurialManager> managers = multipleMercurialRepositoryManager.getActiveRepositoryList();
        for (MercurialManager manager: managers) {
            final ErrorCollection errors = new SimpleErrorCollection();
            // If the repository has local releasenotes email
            // addresses defined, then use them instead of the default
            // ones defined in emailAddresses
            String emailAddresses = manager.getReleasenotesemail();
            if (emailAddresses == null || emailAddresses.equals("")) {
                emailAddresses = defaultEmailAddresses;
            }

            log.warn("Generating release notes for " + manager.getDisplayName() + " with email addresses " + emailAddresses + " and release field name " + addressedInFieldName);            

            // Doesn't update the repository because this is
            // called just after updateIndex() has been called
            String htmlReport =
                reportsResource.getReleaseNotesHtml(false, 
                                                    errors, 
                                                    ReportsResource.PREVIOUS,
                                                    ReportsResource.LATEST,
                                                    ReportsResource.DEDUCE,
                                                    manager.getDisplayName(),
                                                    "",
                                                    emailAddresses,
                                                    null,
                                                    addressedInFieldName);
            if (errors.hasAnyErrors()) {
                // These are errors from the perspective of the plugin but not
                // for JIRA
                log.warn("Problems during report generation: " + errors.getErrorMessages());
            }
        }
        
    }

    public ObjectConfiguration getObjectConfiguration() throws ObjectConfigurationException
    {
        return getObjectConfiguration("HGREVISIONSERVICE", "services/plugins/mercurial/revisionindexservice.xml", null);
    }

    public static void install(ServiceManager serviceManager) throws Exception
    {
        if (serviceManager.getServiceWithName(REVISION_INDEX_SERVICE_NAME) == null)
        {
            serviceManager.addService(REVISION_INDEX_SERVICE_NAME, RevisionIndexService.class, REVISION_INDEX_SERVICE_DELAY);

            // Note: the service wasn't being added and when manually added showed the following problem:
            // Error adding service: org.picocontainer.defaults.UnsatisfiableDependenciesException: net.customware.jira.plugins.ext.mercurial.revisions.RevisionIndexService doesn't have any satisfiable constructors. Unsatisfiable dependencies: [[interface com.atlassian.jira.config.properties.ApplicationProperties, interface com.atlassian.jira.issue.IssueManager, interface com.atlassian.mail.server.MailServerManager, interface com.atlassian.jira.ofbiz.OfBizDelegator, interface com.atlassian.jira.security.PermissionManager, interface com.atlassian.jira.project.ProjectManager, interface com.atlassian.sal.api.user.UserManager, interface com.atlassian.jira.user.util.UserUtil, interface com.atlassian.velocity.VelocityManager, interface com.atlassian.jira.util.I18nHelper$BeanFactory]].
        }
    }

    public static void remove(ServiceManager serviceManager) throws Exception
    {
        if (serviceManager.getServiceWithName(REVISION_INDEX_SERVICE_NAME) != null)
        {
            serviceManager.removeServiceByName(REVISION_INDEX_SERVICE_NAME);
        }
    }

    public static MultipleMercurialRepositoryManager getMultipleMercurialRepositoryManager()
    {
        if (mmrmi == null) {
            mmrmi = new MultipleMercurialRepositoryManagerImpl();
        }
        return mmrmi;
    }

    public boolean isUnique()
    {
        return true;
    }

    public boolean isInternal()
    {
        // Allow it to be deleted
        return false;
    }

    public String getDescription()
    {
        return "This service indexes Mercurial revisions.";
    }
}
