package net.customware.jira.plugins.ext.mercurial.action;

import net.customware.jira.plugins.ext.mercurial.MercurialManager;

import java.util.Iterator;
import java.util.Collection;

public class ActivateMercurialRepositoryAction extends MercurialActionSupport {
    private long repoId;
    private MercurialManager mercurialManager;
    
    public ActivateMercurialRepositoryAction() {
    }
    
    public String getRepoId() {
        return Long.toString(repoId);
    }
    
    public void setRepoId(String repoId) {
        this.repoId = Long.parseLong(repoId);
    }
    
    public String doExecute() {
        if (!hasPermissions())
        {
            return PERMISSION_VIOLATION_RESULT;
        }
        
        mercurialManager = getMultipleRepoManager().getRepository(repoId);
        mercurialManager.activate();
        if (!mercurialManager.isActive()) {
            addErrorMessage(getText("mercurial.repository.activation.failed", mercurialManager.getInactiveMessage()));
        }
        
        return SUCCESS;
    }
    
    /**
     * Activate all repositories
     */
    public String doAll() {
        if (!hasPermissions())
        {
            return PERMISSION_VIOLATION_RESULT;
        }
        
        Collection<MercurialManager> mercurialManagers = getMultipleRepoManager().getRepositoryList();
        for (Iterator it = mercurialManagers.iterator(); it.hasNext(); ) {
            MercurialManager mm = (MercurialManager)it.next();
            if (!mm.isActive()) {
                mm.activate();
                if (!mm.isActive()) {
                    addErrorMessage(getText("mercurial.repository.activation.failed", mm.getDisplayName()));
                }
            }
        }
        
        return "all";
    }
    
    public MercurialManager getMercurialManager() {
        return mercurialManager;
    }
    
}
