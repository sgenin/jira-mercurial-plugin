package net.customware.jira.plugins.ext.mercurial.projecttabpanels;

import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.projectpanel.ProjectTabPanel;
import com.atlassian.jira.plugin.projectpanel.impl.GenericProjectTabPanel;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.browse.BrowseContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.I18nBean;
import net.customware.jira.plugins.ext.mercurial.MercurialManager;
import net.customware.jira.plugins.ext.mercurial.MultipleMercurialRepositoryManager;
import net.customware.jira.plugins.ext.mercurial.revisions.RevisionIndexService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * This class provides a tab panel for the Mercurial Releases view.
 *
 */
public class MercurialProjectReleaseNotesTabPanel extends GenericProjectTabPanel implements ProjectTabPanel
{
    private static final Logger log = Logger.getLogger(MercurialProjectReleaseNotesTabPanel.class);

    private final MultipleMercurialRepositoryManager multipleMercurialRepositoryManager;

    private final PermissionManager permissionManager;

    /**
     *
     */
    public MercurialProjectReleaseNotesTabPanel(JiraAuthenticationContext authenticationContext, PermissionManager permissionManager)
    {
        super(authenticationContext);
        this.permissionManager = permissionManager;
        this.multipleMercurialRepositoryManager = RevisionIndexService.getMultipleMercurialRepositoryManager();
    }

    public String getHtml(BrowseContext browseContext)
    {
    	log.debug("Rendering Mercurial Release notes template for " + browseContext.getProject().getKey());

        Map<String, Object> startingParams = new HashMap<String, Object>();
        Project project = browseContext.getProject();
        ApplicationUser user = browseContext.getUser();
        String key = project.getKey();
        
        startingParams.put("action", getI18nBean(user));
        startingParams.put("action", getI18nBean(user));
        startingParams.put("project", project);
        startingParams.put("projectKey", key);
        startingParams.put("activerepositories", getRepositories(true));

        return descriptor.getHtml("view", startingParams);
    }

    // Just like ViewMercurialRepositoriesAction
    private Collection<MercurialManager> getRepositories(boolean active)
    {
        Collection<MercurialManager> ms;
        if (active) {
            ms = multipleMercurialRepositoryManager.getActiveRepositoryList();
        } else {
            ms = multipleMercurialRepositoryManager.getInactiveRepositoryList();
        }
        List<MercurialManager> mercurialManagers = new ArrayList<MercurialManager>(ms);

        Collections.sort(
                mercurialManagers,
                new Comparator<MercurialManager>()
                {
                    public int compare(MercurialManager left, MercurialManager right)
                    {
                        // Make the inactive ones appear first
                        String l = StringUtils.defaultString(left.getDisplayName());
                        String r = StringUtils.defaultString(right.getDisplayName());
                        return l.compareTo(r);
                    }
                }
        );

        return mercurialManagers;
    }

    I18nBean getI18nBean(ApplicationUser user)
    {
        return new I18nBean(user);
    }

    @Override
    public boolean showPanel(BrowseContext browseContext)
    {
        return multipleMercurialRepositoryManager.isIndexingRevisions() &&
            permissionManager.hasPermission(ProjectPermissions.VIEW_DEV_TOOLS, browseContext.getProject(), browseContext.getUser());
    }
}
