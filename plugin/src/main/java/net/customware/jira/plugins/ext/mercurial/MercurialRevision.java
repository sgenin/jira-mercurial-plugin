package net.customware.jira.plugins.ext.mercurial;

import java.util.Date;

public class MercurialRevision
{
    private long revisionNumber;
    private String message;
    private Date date;
    private String author;
    private String tags;

    public long getRevisionNumber()
    {
        return revisionNumber;
    }

    public String getMessage()
    {
        return message;
    }

    public Date getDate()
    {
        return date;
    }

    public String getAuthor()
    {
        return author;
    }

    public String getTags()
    {
        return tags;
    }

    public void setRevisionNumber(long revisionNumber)
    {
        this.revisionNumber = revisionNumber;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public void setTags(String tags)
    {
        this.tags = tags;
    }
}
