/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Sep 30, 2004
 * Time: 8:14:04 AM
 */
package net.customware.jira.plugins.ext.mercurial;

import net.customware.jira.plugins.ext.mercurial.linkrenderer.MercurialLinkRenderer;
import net.customware.hg.core.io.HGLogEntry;

import com.opensymphony.module.propertyset.PropertySet;

import java.util.Collection;

public interface MercurialManager {
    void activate();
    void deactivate(String message);

    String getClonedir();
    String getReleasenotesemail();
    String getReleasenoteslatest();
    String getDisplayName();
    long getId();
    String getInactiveMessage();
    MercurialLinkRenderer getLinkRenderer();
    Collection getLogEntries(long revision);
    HGLogEntry getLogEntry(long revision);
    String getPassword();
    String getPrivateKeyFile();
    PropertySet getProperties();
    String getRepositoryUUID();
    int getRevisioningCacheSize();
    String getRoot();
    String getSubreposcmd();
    String getUsername();
    ViewLinkFormat getViewLinkFormat();

    boolean isActive();
    boolean isRevisionIndexing();
    boolean isSubrepos();

    void recordRelease(String artifactId, String label);

    void update(HgProperties properties);
}
