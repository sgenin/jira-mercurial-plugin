package net.customware.jira.plugins.ext.mercurial.action;

import net.customware.jira.plugins.ext.mercurial.MercurialManager;

import java.util.Iterator;
import java.util.Collection;

public class DeactivateMercurialRepositoryAction extends MercurialActionSupport {
    private long repoId;
    private MercurialManager mercurialManager;
    
    public DeactivateMercurialRepositoryAction() {
    }
    
    public String getRepoId() {
        return Long.toString(repoId);
    }
    
    public void setRepoId(String repoId) {
        this.repoId = Long.parseLong(repoId);
    }
    
    /**
     * Deactivate one repository based on repoId
     */
    public String doExecute() {
        if (!hasPermissions())
        {
            return PERMISSION_VIOLATION_RESULT;
        }
        
        mercurialManager = getMultipleRepoManager().getRepository(repoId);
        mercurialManager.deactivate("explicitly deactivated");
        if (mercurialManager.isActive()) {
            addErrorMessage(getText("mercurial.repository.deactivation.failed"));
        }
        
        return SUCCESS;
    }
    
    /**
     * Deactivate all repositories
     */
    public String doAll() {
        if (!hasPermissions())
        {
            return PERMISSION_VIOLATION_RESULT;
        }
        
        Collection<MercurialManager> mercurialManagers = getMultipleRepoManager().getRepositoryList();
        for (Iterator it = mercurialManagers.iterator(); it.hasNext(); ) {
            MercurialManager mm = (MercurialManager)it.next();
            if (mm.isActive()) {
                mm.deactivate("all repositories explicitly deactivated");
                if (mm.isActive()) {
                    addErrorMessage(getText("mercurial.repository.deactivation.failed"));
                }
            }
        }
        
        return "all";
    }
    
    public MercurialManager getMercurialManager() {
        return mercurialManager;
    }
    
}
