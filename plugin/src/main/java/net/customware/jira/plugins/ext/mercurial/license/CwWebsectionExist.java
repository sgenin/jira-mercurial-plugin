package net.customware.jira.plugins.ext.mercurial.license;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginParseException;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertyException;
import com.opensymphony.module.propertyset.PropertySetManager;

import java.util.HashMap;
import java.util.Map;

public class CwWebsectionExist implements com.atlassian.plugin.web.Condition {

	protected PluginAccessor pluginAccessor;
	
	private static PropertySet ofbizPs = null;
	
	private static final String CW_PLUGIN_KEY = "net.customware.jira.plugins.mercurial-jira-plugin";
	private static final String CW_WEB_SECTION = "cwjira-admin";
	private static final int CW_WEB_SECTION_ENTITY_ID = 110011;

	public CwWebsectionExist(PluginAccessor pluginAccessor) {
		this.pluginAccessor = pluginAccessor;
	}
        
	@Override
	public void init(Map<String, String> arg0) throws PluginParseException {} 

	@Override
	public boolean shouldDisplay(Map<String, Object> arg0) {
		try {
            String webKey = getPS().getString(CW_WEB_SECTION);
            
            if (webKey != null && webKey != "") {	// make sure there are value returned before checking for the plugin
            	if (pluginAccessor.getEnabledPlugin(webKey) != null) {	// check whether the plugin is enabled
            		if (webKey.equals(CW_PLUGIN_KEY)) {
            			return true;
            		} else {
            			return false;
            		}
            	}
            }
        } catch (PropertyException pe) {
            // No such value in the database yet
        }
        
        setCwWebSectionKey();	// nothing in the database, so set the value
		return true;
	}
	
	private static PropertySet getPS() {
        if (ofbizPs == null) {
            HashMap ofbizArgs = new HashMap();
            ofbizArgs.put("delegator.name", "default");
            ofbizArgs.put("entityName", "cw-menu-section.properties");
            ofbizArgs.put("entityId", new Long(CW_WEB_SECTION_ENTITY_ID));
            ofbizPs = PropertySetManager.getInstance("ofbiz", ofbizArgs);
        }
        return ofbizPs;
    }
    
    private static void setCwWebSectionKey() {
    	getPS().setString(CW_WEB_SECTION, CW_PLUGIN_KEY);
    }
}

